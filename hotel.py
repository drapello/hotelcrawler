from datetime import datetime
from mongomodel import MongoCollection
from crawl import Crawler
from lxml import html as lhtml
import unicodedata
from datetime import datetime
import re
from re import sub

class Cidade(MongoCollection, Crawler):
    id = None
    nome = None
    uf = None
    hoteis = None
    url = None
    last_scan_date = None


    def __init__(self, id=None, nome=None, uf=None, hoteis=[], last_scan_date=None):
        self.id = id
        self.nome = nome
        self.uf = uf
        self.hoteis = hoteis
        self.last_scan_date = last_scan_date


    def page_parse(self,post):
        html,headers = Crawler().get(self.url,post)
        doc = lhtml.fromstring(html)

        for h in doc.cssselect('.resultados .anuncio0'):
            nome = h.cssselect('td')[2].text_content()

            try:
                price = h.cssselect('td')[1].text_content()
                if price:
                    price = sub(r'[^\d,]', '', price).replace(",",".")
                url = h.cssselect('td')[2].cssselect('a')[0].get('href')
                m = re.search('(?<=busca=)\d+', url)            
                id = m.group(0)

                hotel = Hotel(id=id, 
                        nome = nome,
                        uf = self.uf,
                        cidade = self.nome,
                        price = price,
                        url = url,
                        last_scan_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                  )
                hotel.parse()
                hotel.save_or_update({"id": hotel.id})
                self.hoteis.append(hotel.to_dict())
            except Exception, e:
                print "skipping hotel %s " % (unicodedata.normalize("NFKD", unicode(self.nome)).encode("ASCII", "ignore"))



    def parse(self):
        self.url = "http://www.hotelinsite.com.br/resultado.asp?" + self.id
        html,headers = Crawler().get(self.url)
        doc = lhtml.fromstring(html)
        paginacao = doc.cssselect('.paginacao ul li')
        if paginacao:
            for page in paginacao:
                self.page_parse("pag_num=%s&loc_tp=cid&loc_id=%s" % (page.text_content(), self.id))
        else:
            resultados = doc.cssselect('.resultados')
            if resultados:
                self.page_parse(None)


    def find(self, parameters=None):
        cursor = self.findOne(parameters)
        self.id = cursor['id']
        self.nome = cursor['nome']
        self.uf = cursor['uf']
        self.hoteis = cursor['hoteis']
        self.url = cursor['url']
        self.last_scan_date = cursor['last_scan_date']

class Hotel(MongoCollection, Crawler):

    id = None
    nome = None
    uf = None
    cidade = None
    price = 0.0
    url = None
    last_scan_date = None

    def __init__(self, id=None, nome=None, uf=None, cidade=None, price=None, url=None, last_scan_date=None):
        self.id = id
        self.nome = nome
        self.uf = uf
        self.cidade = cidade
        self.price = price
        self.url = url
        self.last_scan_date = last_scan_date


    def clean(self, data):
        data = data.replace(":","")
        data = data.split("/")[0]
        data = data.rstrip()    
        data = data.lower()
        data = data.replace(" ", "-")
        data = unicodedata.normalize("NFKD", unicode(data)).encode("ASCII", "ignore")   
        return str(data)

    def parse(self):
        html,headers = Crawler().get(self.url)
        doc = lhtml.fromstring(html)
        for tr in doc.cssselect('.hotel-data tr'):
            if len(tr.cssselect('td'))>1:
                key = tr.cssselect('td')[0].text_content()
                if key:
                    key = self.clean(key)
                    if key == "no-hotel":
                        key = "facilidade-hotel"
                    if key == "website":
                        if tr.cssselect('td')[1].cssselect('a'):
                            value = tr.cssselect('td')[1].cssselect('a')[0].get('href')
                    else:
                        value = tr.cssselect('td')[1].text_content()

                    self.__setattr__(key,value)
        print "hotel %s " % (self.nome)

