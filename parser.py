
from crawl import Crawler
from lxml import html as lhtml
import re
from hotel import *

html,headers = Crawler().get("http://www.hotelinsite.com.br/hoteis_no_brasil.asp")
doc = lhtml.fromstring(html)
for uf in doc.cssselect('.lista-mapa li'):
	url = uf.cssselect('a')[0].get('href')
	m = re.search('(?<=uf=)\w+', url)
	uf_sigla = m.group(0)

	html,headers = Crawler().get("http://www.hotelinsite.com.br/hoteis_no_brasil.asp?uf="+uf_sigla)
	doc = lhtml.fromstring(html)
	for cidade in doc.cssselect('.mark')[0].cssselect('li'):
		cidade_link = cidade.cssselect('a')[0].get('href')
		m = re.search('(?<=asp\?)\d+', cidade_link)
		cidade_id = m.group(0)
		cidade_nome = cidade.text_content()
		cidade = Cidade(id=cidade_id,
						nome=cidade_nome,
						uf=uf_sigla)
		cidade.save()
		print "Crawling city %s in %s" % (cidade_nome, uf_sigla)