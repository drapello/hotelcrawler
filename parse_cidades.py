from hotel import *


while True:
	last_scan_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")
	cidade = Cidade()
	lock = cidade.lock({"last_scan_date": ""}, {"last_scan_date": last_scan_date})

	if lock['updatedExisting']:
		cidade.find({"last_scan_date": last_scan_date})
		print "Crawling hoteis in city %s" % (cidade.nome)
		cidade.parse()
		cidade.save_or_update({"last_scan_date": last_scan_date})
		print "finish \n"
	else:
		break
